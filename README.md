# dotfiles
The personal dotfiles of Mark Bluemer managed using [chezmoi](https://www.chezmoi.io/).

## How to Use
First, generate a new ssh key
```sh
ssh-keygen -t ed25519 -C "<insert email>"
cat ~/.ssh/id_ed25519.pub | pbcopy
```

and add to Gitlab account. Then run:
```sh
sh -c "$(curl -fsLS get.chezmoi.io)" -- init --apply --ssh gitlab.com/mbluemer/dotfiles
```
