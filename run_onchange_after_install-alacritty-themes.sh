#!/bin/bash

if [ ! -d ~/.config/alacritty/catppuccin ]; then
	git clone https://github.com/catppuccin/alacritty.git ~/.config/alacritty/catppuccin
fi
